Programs_Closed-loop_phototaxis
---
Contains Matlab scripts and functions for closed-loop orientational phototaxis experiments
---

Folder 'experiments' contains main scripts and all functions for closed-loop experiments

Stereo-visual experiments :
- Main_orientation_lateralisation

Temporal experiments :
- Main_orientation_exponential.m (exponential profiles)
- Main_orientation_sinusoidal.m (sinusoidal profiles)

Folder 'illumination' contains 
- a calibration script for the projector used
- .mat files for calibration